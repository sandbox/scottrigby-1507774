The master branch is intentionally empty. Please clone a proper version branch
like '7.x-2.x' or '6.x-2.x'. For instructions see the project's Git tab at
http://drupal.org/project/1507774/git-instructions.
